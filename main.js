var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var apiResult = JSON.parse(this.responseText);

        //Code Dealing With The API Data Goes Here
        console.log(apiResult);

        var base_layer = apiResult.base_layer.name;
        var condiment = apiResult.condiment.name;
        var mixin = apiResult.mixin.name;
        var seasoning = apiResult.seasoning.name;
        var shell = apiResult.shell.name;

        var ingredients_list = document.getElementById('taco-wrapper').children;
        // console.log(ingredients_list);


        var ingredient_array = [base_layer, condiment, mixin, seasoning, shell]

        for(i=0; i<ingredients_list.length; i++){
            ingredients_list[i].children[0].innerHTML = ingredient_array[i];
        }

        //below does the same as the for loop above
        // ingredients_list[0].children[0].innerHTML = base_layer;
        // ingredients_list[1].children[0].innerHTML = condiment;
        // ingredients_list[2].children[0].innerHTML = mixin;
        // ingredients_list[3].children[0].innerHTML = seasoning;
        // ingredients_list[4].children[0].innerHTML = shell;

    }
};
xmlhttp.open('GET', 'http://taco-randomizer.herokuapp.com/random/', true);
xmlhttp.send();

// all code above (inside {}) you do not have access to down here

var taco_wrapper = document.getElementById('taco-wrapper');
var button = document.getElementById('button');

function toggleDropdown() {
    taco_wrapper.classList.toggle('show');
}

button.addEventListener('click', toggleDropdown);